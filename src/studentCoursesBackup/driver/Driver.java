package studentCoursesBackup.driver;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.FileProcessor;
import studentCoursesBackup.util.Results;
import studentCoursesBackup.util.TreeBuilder;

/**
 * Driver class to read and use arguments
 * Checks for arguments and reads in input files and delete files
 * Outputs to three output files
 */
public class Driver {
    public static void main(String[] args) {
        // check: input.txt, delete.txt, output1.txt, output2.txt, output3.txt
        if(!(args[0].equals("input.txt") &&
        	 args[1].equals("delete.txt") &&
        	 args[2].equals("output1.txt") && 
        	 args[3].equals("output2.txt") && 
        	 args[4].equals("output3.txt"))) {
    	 	System.err.println(
    	 		"Error: Arguments fail to meet criteria: " +
    	 		"Must be: input.txt, delete.txt, " +
    	 		"output1.txt, output2.txt, output3.txt");
    	 	System.exit(0);
        }
        
		// setup Trees and Results
		TreeBuilder mainTree = new TreeBuilder();
		TreeBuilder backUpTree1 = new TreeBuilder();
		TreeBuilder backUpTree2 = new TreeBuilder();
		
		Results mainResults = new Results(args[2]);
		Results backUpResults1 = new Results(args[3]);
		Results backUpResults2 = new Results(args[4]);
		
        // Adding nodes to the three trees using input file
        FileProcessor fp = new FileProcessor();
        String fileName = args[0];
        int lineNumber = 0;
	    String nextLine = fp.readLine(fileName, lineNumber++);
	    while(nextLine != null) {
    	    String[] parameters = nextLine.trim().split(":");
    	    int bNumber = Integer.parseInt(parameters[0]);
    	    String course = parameters[1];
    	    int courseValue = (int) course.charAt(0);
    	    if(courseValue >= (int) 'A' && courseValue <= (int) 'K') {
    	        Node newNode = new Node(bNumber);
    	        try {
	    	        Node backUpNode1 = newNode.clone();
	    	        Node backUpNode2 = newNode.clone();
	    	        newNode.addBackUp(backUpNode1);
	    	        newNode.addBackUp(backUpNode2);
	    	        
	    	        mainTree.addNode(newNode, mainTree.getRoot());
	    	        backUpTree1.addNode(backUpNode1, backUpTree1.getRoot());
	    	        backUpTree2.addNode(backUpNode2, backUpTree2.getRoot());
	    	        
	    	        mainTree.addCourse(bNumber, course);
    	        } catch (CloneNotSupportedException e) {
    	        	System.err.println(
    	        		"Error: Failed to clone Node: " + e);
    	        	System.exit(0);
    	        }
    	    }
	    	nextLine = fp.readLine(fileName, lineNumber++);
	    }
	    
	    // Deleting courses from nodes in the three trees using delete file
	    fileName = args[1];
	    lineNumber = 0;
	    nextLine = fp.readLine(fileName, lineNumber++);
	    while(nextLine != null) {
    	    String[] parameters = nextLine.trim().split(":");
    	    int bNumber = Integer.parseInt(parameters[0]);
    	    String course = parameters[1];
    	    int courseValue = (int) course.charAt(0);
    	    if(courseValue >= (int) 'A' && courseValue <= (int) 'K') {
    	        mainTree.deleteCourse(bNumber, course);
    	    }
	    	nextLine = fp.readLine(fileName, lineNumber++);
	    }

	    // Output nodes to Results
	    mainTree.printNodes(mainResults, mainTree.getRoot());
	    backUpTree1.printNodes(backUpResults1, backUpTree1.getRoot());
	    backUpTree2.printNodes(backUpResults2, backUpTree2.getRoot());
	    
	    // Send results to files
	    mainResults.outputToFile();
	    backUpResults1.outputToFile();
	    backUpResults2.outputToFile();
	    
	    // Close file writers
	    mainResults.closeFileWriter();
	    backUpResults1.closeFileWriter();
	    backUpResults2.closeFileWriter();
    }
}

