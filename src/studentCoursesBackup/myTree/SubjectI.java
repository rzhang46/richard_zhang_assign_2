package studentCoursesBackup.myTree;

/**
 * Interface that is followed by Observer Interface
 * Can add additional Observers and update all of them
 */
public interface SubjectI {
    /**
     * Adds new observer Node to Subject Node
     */
    public void registerObserver(Node newObserver);
    
    /**
     * Tells each observer Node to update
     */
    public void alertAll();
}