package studentCoursesBackup.myTree;

/**
 * Interface that follows a single Subject Interface
 * Calls update when Subject Interface signals an update
 */
public interface ObserverI {
    /**
     * Copies over the updated course information to the backup
     */
    public void update(Node backUpNode, Node updatedNode);
}