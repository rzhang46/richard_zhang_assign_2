package studentCoursesBackup.myTree;

import java.util.ArrayList;

/**
 * Node class that is used for each b-number
 * 
 * param bNumber    a 4-digit integer
 * param courses    a list of letters ranging from 'A' to 'K'
 * param left       points to the left subtree Node with a lesser bNumber
 * param right      points to the right subtree Node with a greater bNumber
 * param backUps    a list of Nodes that are clones of this Node object
 */
public class Node implements Cloneable, ObserverI, SubjectI {
    private final int bNumber; // 4 digit int
    private ArrayList<String> courses; // possible course names are "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"
    private Node left;
    private Node right;
    private ArrayList<Node> backUps;

    /**
     * Constructor: assigns only the b-number, all else defaults
     */
    public Node(int abNumber) {
        bNumber = abNumber;
        courses = new ArrayList<>();
        left = null;
        right = null;
        backUps = new ArrayList<>();
    }
    
    // Getters
    public int getbNumber() {
        return bNumber;
    }
    
    public ArrayList<String> getCourses() {
        return courses;
    }
    
    public Node getLeft() {
        return left;
    }
    
    public Node getRight() {
        return right;
    }
    
    public ArrayList<Node> getBackUps() {
        return backUps;
    }
    
    // Setters and Mutators
    public void setLeft(Node newNode) {
        left = newNode;
    }
    
    public void setRight(Node newNode) {
        right = newNode;
    }
    
    /**
     * Only adds the course if it does not exist in the list
     */
    public void addCourse(String course) {
        boolean courseExists = false;
        for(String i : courses) {
            if(i.equals(course)) {
                courseExists = true;
                break;
            }
        }
        if(!courseExists) {
            courses.add(course);
        }
    }
    
    /**
     * Tries to remove the given course from the course list
     */
    public void removeCourse(String course) {
        courses.remove(course);
    }
    
    /**
     * Removes all courses from the course list
     */
    public void emptyCourses() {
        courses.clear();
    }
    
    /**
     * Adds the backup node to the backup list
     */
    public void addBackUp(Node newBackUp) {
        backUps.add(newBackUp);
    }
    
    /**
     * Copies over details of the updated Node to the backup node
     * Currently only copies over the course list
     */
    public void update(Node backUpNode, Node updatedNode) {
        backUpNode.emptyCourses();
        ArrayList<String> updatedCourses = updatedNode.getCourses();
        for(String i : updatedCourses) {
            backUpNode.addCourse(i);
        }
    }
    
    /**
     * Signals all Nodes in the backup list to copy the updated Subject Node
     */
    public void alertAll() {
        ArrayList<Node> backUpList = getBackUps();
        for(Node i : backUpList) {
            update(i, this);
        }
    }
    
    /**
     * Adds the Observer to the backup list
     */
    public void registerObserver(Node newObserver) {
        addBackUp(newObserver);
    }
    
    /**
     * Simply makes a copy of this Node
     * return Node with the same b-number to be used in backup trees
     */
    @Override
    public Node clone() throws CloneNotSupportedException {
        Node newNode = new Node(getbNumber());
        return newNode;
    }
}