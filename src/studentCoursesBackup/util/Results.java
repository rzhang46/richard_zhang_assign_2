package studentCoursesBackup.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Stores the result of a test and outputs it to stdout or a file
 */
public class Results implements 
        FileDisplayInterface, 
        StdoutDisplayInterface {
    private ArrayList<String> nodes;
    private String fileName;
    private BufferedWriter bufferedWriter;
    private FileWriter fileWriter;
    
    /**
     * Constructor
     */
    public Results(String newFileName) {
        nodes = new ArrayList<>();
        fileName = newFileName;
        try {
            fileWriter = new FileWriter(newFileName);
            bufferedWriter = new BufferedWriter(fileWriter);
        } catch (IOException e) {
            System.err.println(
                "Failed to open file: " + newFileName + " for writing");
        }
    }
    
    // Getters
    public String getFileName() {
        return fileName;
    }
    
    public void addNode(String node) {
        nodes.add(node);
    }
    
    /**
     * Closes the file writer
     */
    public void closeFileWriter() {
        try {
            bufferedWriter.close();
        } catch (IOException e) {
            System.err.println("Failed to close FileWriter: " + e);
        }
    }
    
    /**
     * Outputs the string to file
     */
    public void writeToFile(String s) {
        try {
            bufferedWriter.write(s);
        } catch (IOException e) {
            System.err.println("Failed to write to output file: " + e);
        }
    }
    
    /**
     * Outputs the string to stdout
     */
    public void writeToStdout(String s) {
        System.out.print(s);
    }

    /**
     * Writes all strings in the arraylist to the file
     */
    public void outputToFile() {
        for(String i : nodes) {
            writeToFile(i);
        }
    }
    
    /**
     * Writes all strings in the arraylist to stdout
     */
    public void outputToStdout() {
        for(String i : nodes) {
            System.out.println(i);
        }
    }
}