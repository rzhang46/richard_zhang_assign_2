package studentCoursesBackup.util;

/**
 * Interface that allows writing to standard output
 */
interface StdoutDisplayInterface {
    /**
     * Prints the string to stdout
     */
    void writeToStdout(String s);
}