package studentCoursesBackup.util;

/**
 * Interface that outputs strings to a file
 */
interface FileDisplayInterface {
    /**
     * Writes the string to a file
     */
    void writeToFile(String s);
}