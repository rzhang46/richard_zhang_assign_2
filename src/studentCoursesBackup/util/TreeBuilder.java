package studentCoursesBackup.util;

import studentCoursesBackup.myTree.Node;
import studentCoursesBackup.util.Results;

/**
 * Tree class for Nodes
 * Used to clone observers for each subject Node
 */
public class TreeBuilder {
    private Node root;
    
    // Default constructor
    public TreeBuilder() {
        root = null;
    }
    
    // Getters
    public Node getRoot() {
        return root;
    }
    
    // Setters
    public void setRoot(Node newNode) {
        root = newNode;
    }
    
    /**
     * Recursively adds a new node to the tree
     * Does nothing if a node with same bNumber already exists
     */
    public void addNode(Node newNode, Node root) {
        if(root == null) {
            setRoot(newNode);
        } else if(newNode != null) {
            if(newNode.getbNumber() < root.getbNumber()) {
                if(root.getLeft() == null) {
                    root.setLeft(newNode);
                } else {
                    addNode(newNode, root.getLeft());
                }
            } else if(newNode.getbNumber() > root.getbNumber()) {
                if(root.getRight() == null) {
                    root.setRight(newNode);
                } else {
                    addNode(newNode, root.getRight());
                }
            }
        }
    }
    
    /**
     * Finds the node with the associated b-number
     * return Node if found, else null
     */
    public Node getNode(int bNumber, Node root) {
        if(root == null) {
            return null;
        }
        if(bNumber == root.getbNumber()) {
            return root;
        } else if(bNumber < root.getbNumber()) {
            return getNode(bNumber, root.getLeft());
        } else {
            return getNode(bNumber, root.getRight());
        }
    }
    
    /**
     * Tries to add the course for the given b-number
     */
    public void addCourse(int bNumber, String course) {
        Node targetNode = getNode(bNumber, getRoot());
        if(targetNode != null) {
            targetNode.addCourse(course);
            targetNode.alertAll();
        }
    }
    
    /**
     * Tries to remove the course for the Node's list
     */
    public void deleteCourse(int bNumber, String course) {
        Node targetNode = getNode(bNumber, getRoot());
        if(targetNode != null) {
            targetNode.removeCourse(course);
            targetNode.alertAll();
        }
    }
    
    /**
     * Sends the b-number and courses of each Node to a Results instance
     * Nodes are sorted in order by b-number (in-order traversal)
     */
    public void printNodes(Results results, Node root) {
        if(root != null) {
            Node leftSubtree = root.getLeft();
            Node rightSubtree = root.getRight();
            
            printNodes(results, leftSubtree);
            
            StringBuilder nodeInformation = new StringBuilder();
            nodeInformation.append(root.getbNumber());
            for(String i : root.getCourses()) {
                nodeInformation.append(" " + i);
            }
            nodeInformation.append("\n");
            results.addNode(nodeInformation.toString());
            
            printNodes(results, rightSubtree);
        }
    }
}