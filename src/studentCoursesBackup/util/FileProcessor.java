package studentCoursesBackup.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Used to read lines from an input file
 */
public class FileProcessor {
    public FileProcessor() {}
    
    /**
     * reads a single line from a file
     * @return the line
     */
    public String readLine(String file, int lineNumber) {
        String line = null;
        FileReader fileReader = null;
        BufferedReader bufferReader = null;
        try {
            fileReader = new FileReader(file);
            bufferReader = new BufferedReader(fileReader);
            int count = 0;
            while(count <= lineNumber) {
                line = bufferReader.readLine();
                if(line == null) {
                    break;
                }
                count++;
            }
        } catch (java.io.FileNotFoundException e) {
            System.err.println("Error: input file not found");
        } catch (IOException e) {
            System.err.println("Error: IOException while reading file");
        } finally {
            try {
                if(bufferReader != null) {
                    bufferReader.close();
                }
            } catch (IOException e) {
                System.err.println("Error: cannot close invalid reader");
            }
        }
        return line;
    }
}