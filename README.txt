Richard Zhang
rzhang46@binghamton.edu

## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml all

-----------------------------------------------------------------------
## To run:
example: ant -buildfile src/build.xml run -Darg0=input.txt -Darg1=delete.txt -Darg2=output1.txt -Darg3=output2.txt -Darg4=output3.txt

-----------------------------------------------------------------------
"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

[Date: 10/2/17]

-----------------------------------------------------------------------
Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

I used a binary search tree for each Node tree. The nodes are placed in 
based on b-number. On average cases, finding a node with a certain b-number 
will be O(log n), but can be linear depending on how balanced input file is.

-----------------------------------------------------------------------
Observer Pattern

The observer pattern has been implemented when creating a new node. That
new node will be cloned twice and then their references will be appended
to the original node's backup list. Finally, each of these three nodes 
will be added to their respective trees.

When adding or removing a course from a node, this is initially only done 
to the original node. At the end of the function (add/delete course), it 
calls another function to notify all backup nodes, making each of them 
call update. The update function ensures an original node and its backups 
have the same course list.
